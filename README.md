**Note: I developed the package here, but the latest version is published on [github](https://www.github.com/mweylandt/straightliner)**

 <img src="man/figures/logo.png" align="right" height="138" />

# Straightlining

Implements the quantitative measures outlined by [Kim et al 2019](https://doi.org/10.1177/0894439317752406). The package includes measures for straighlining on the individual level, but can also generate a report on how much straightlining happens across a battery of questions.



## Installation:
```{r}
devtools::install_gitlab("mweylandt/straightliner")
```

## Usage

For seeing which respondents are straightlining, use `straightlining`

```r
survey <- data.frame("respondent" = c("A", "B", "C"),
"item1" = c(5, 5, 4),
"item2" = c(5, 5, 2),
"item3" = c(5, 5, 5),
"item4" = c(5, 1, 3))

st_results <- straightlining(survey, varnames = c("item1", "item3" "item4"))

```
This returns a dataframe where each row is a respondent and each column how they performed on a particular measure. You can cbind this to your original dataset if you want. Alternatively, you can pass the argument `keep_original = TRUE` to return your original dataset plus these measures.

To compare batteries of questions to one another, use the `straightling_report()` function. This one needs a named list with one entry for each battery, so that it can output a decent table:

```
straightlining_report(survey, batteries, measures = c("mrp", "spv"))
```

## measures
For details on the measures call `?straightlining()` to pull up the documentation and see the 'details' section.


## Notes

both `straightlining` and `straightling_report` automatically convert the passed data to numeric if it is not -- these calculations don't work otherwise. They will warn you if this happens, and let you know how many levels are present in the data for each variable. If you design a battery where all responses have the same amount of levels in the survey (say a 5-point likert) but on one of the questions people only pick the bottom 4, then the conversion to numbers will cause a misalignment across this battery and render these statistics _useless_.

# Todo

package help page - explain source, show basic usage and tell where to go look for details on calculation


## convert data
 on each function, read in data set and do some checks to make sure it will all work:
	✓ remove labels
	✓ convert factors to numeric?
	✓ convert characters to numeric?
  	✓ improve print so it prints named vector and not just the names
	+ give warnings if the conversion doesn't work -- i.e. make sure the TryCatch situation works.

## Roadmap

- This package has some different functions than `careless`, which is an excellent R package. With time I hope to integrate their straighlining measures here too, to make it a one-stop shop.
- Examples in the docs won't work

